title: Deposits
---
sort_key: 132
---
summary: Deposits
---
allow_comments: no
---
body:


The deposits menu prior April, 2023 was not good. We needed something much simpler to manage deposits, a way that requires a minimum of work from the user.

It turns out that our Reception menu and the ability to export data from the stock play well together.

Often, all the books from a tiny publisher will be in deposits. Often,
the publisher also takes care of the books and periodically asks data
to the bookstore: "how many of our books do you currently have in
stock?" and "how many of our books did you sell since last time we did
the check together?".

## Create a new deposit

We will register all books of a deposit in its own shelf: create a
shelf, named "deposit for Tiny Publisher". That's it! We can register
books with the same workflow, and we can easily extract data as we'll see.

NB: this shelf for the deposit is internal to the software. The books have the right to be placed inside other shelves in the physical bookstore.

## Register books

We can thus use the
Reception menu, for the first inventory, or during next inventories,
and scan the books there, like any other book. They don't need special
treatment.

## Get the state of a deposit

Go to the My Stock menu. Select the deposit's shelf. The Export -> CSV option already answers the question "how many of our books do you have in stock". We added another option: "**Export -> CSV & sells…**". Choose two dates and validate. You get another column inside the CSV file that tells us how many copies were sold for each book between those two dates.

The old interface, available on the `/en/deposits` URL, is still there but planned to deletion.
