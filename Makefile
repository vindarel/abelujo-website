run:
	lektor server

img:
	cp content/*.png public/es/
	cp content/*.png public/en/
	cp content/*.png public/de/

build:
	# lektor build -f webpack -O public
	lektor build -O public # goes to ~/.cache/lektor/builds/xxx by default
	make img

publish: build rsync

# use when called -f webpack locally
rsync:
	# publish on abelujo.cc
	chmod 777 -R public/
	@rsync -avzr public/ $(ABELUJO_USER)@$(ABELUJO_SERVER):$(ABELUJO_WEBSITE_HTDOCS)

ssh:
	# j'ai ajouté ma clef publique dans les authorized keys \o/
	@ssh -Y $(ABELUJO_USER)@$(ABELUJO_SERVER)
